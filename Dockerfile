FROM wordpress:4-php7.0-apache

ENV WOOCOMMERCE_VERSION 3.3.1

RUN apt-get update \
    && apt-get install -y apt-utils sudo less mysql-client unzip wget apt-utils \
    && wget https://downloads.wordpress.org/plugin/woocommerce.$WOOCOMMERCE_VERSION.zip -O /tmp/temp.zip \
    && cd /usr/src/wordpress/wp-content/plugins \
    && unzip /tmp/temp.zip \
    && rm /tmp/temp.zip