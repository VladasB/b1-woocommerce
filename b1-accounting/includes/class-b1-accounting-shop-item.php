<?php

/**
 * @since 2.0.0
 * @package B1_Accounting
 * @subpackage B1_Accounting/includes
 * @author B1.lt <info@b1.lt>
 * @link https://www.b1.lt
 */
class B1_Accounting_Shop_Item
{

    public static function set_null_reference_id_product()
    {
        global $wpdb;
        $sql = "
UPDATE {$wpdb->prefix}posts
SET `b1_reference_id` = NULL
WHERE `post_type` = 'product' OR `post_type` = 'product_variation'";
        $wpdb->query($sql);
    }

    public static function get_items_sku()
    {
        global $wpdb;
        $sql = "
SELECT * FROM {$wpdb->prefix}postmeta
WHERE `meta_key` = '_sku'";

        return $wpdb->get_results($sql);

    }

    /**
     * @param string $code
     * @param int $b1_item_id
     */
    public static function update_code_using_b1_item_id($b1_item_id, $code)
    {
        global $wpdb;
        $sql = "
UPDATE {$wpdb->prefix}posts p
LEFT JOIN {$wpdb->prefix}postmeta pm ON pm.post_id = p.ID
SET `b1_reference_id` = %d
WHERE `meta_key` = '_sku' AND `meta_value` = %s";
        $query = $wpdb->prepare($sql, $b1_item_id, $code);
        $wpdb->query($query);
    }

    /**
     * @param int $quantity
     * @param int $b1_item_id
     */
    public static function update_quantity_using_b1_item_id($quantity, $b1_item_id)
    {
        global $wpdb;
        $sql = "
UPDATE {$wpdb->prefix}posts
LEFT JOIN {$wpdb->prefix}postmeta ON post_id = ID
SET `meta_value` = %d
WHERE `meta_key` = '_stock' AND `b1_reference_id` = %d";
        $query = $wpdb->prepare($sql, $quantity, $b1_item_id);
        $wpdb->get_results($query);
    }

    /**
     * @param int $post_id
     *
     * @return int
     */
    public static function get_b1_item_id($post_id)
    {
        global $wpdb;
        $sql = "SELECT b1_product_id FROM {$wpdb->prefix}b1_item_links WHERE `shop_product_id` = %d";
        $query = $wpdb->prepare($sql, $post_id);
        $data = $wpdb->get_var($query);
        if ($data === null) {
            return 0;
        } else {
            return intval($data);
        }
    }

    public static function get_product_reference_id($product_id)
    {
        global $wpdb;
        $sql = "SELECT b1_reference_id FROM {$wpdb->prefix}posts WHERE `ID` = %d";
        $query = $wpdb->prepare($sql, $product_id);

        return $wpdb->get_var($query);
    }

    public static function get_variation_reference_id($variationId)
    {
        global $wpdb;
        $sql = "SELECT b1_reference_id FROM {$wpdb->prefix}posts WHERE `ID` = %d";
        $query = $wpdb->prepare($sql, $variationId);

        return $wpdb->get_var($query);
    }


}
