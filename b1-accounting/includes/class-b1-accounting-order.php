<?php

/**
 * @since 2.0.0
 * @package B1_Accounting
 * @subpackage B1_Accounting/includes
 * @author B1.lt <info@b1.lt>
 * @link https://www.b1.lt
 */
class B1_Accounting_Order {

	public static function assignB1OrderId( $b1OrderId, $orderId ) {
		global $wpdb;
		$sql   = "UPDATE {$wpdb->prefix}posts SET b1_reference_id = %d WHERE ID = %d";
		$query = $wpdb->prepare( $sql, $b1OrderId, $orderId );
		$wpdb->query( $query );
	}

	public static function fetch_all_orders( $sync_from, $iteration ) {
		global $wpdb;
		$sql   = "SELECT * FROM {$wpdb->prefix}posts WHERE b1_reference_id IS NULL AND post_type = 'shop_order' AND `post_status` = 'wc-completed'  AND `post_date` >= %s LIMIT %d";
		$query = $wpdb->prepare( $sql, $sync_from, $iteration );

		return $wpdb->get_results( $query );
	}

}
