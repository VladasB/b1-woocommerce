<?php

/**
 * @since 2.0.0
 * @package B1_Accounting
 * @subpackage B1_Accounting/includes
 * @author B1.lt <info@b1.lt>
 * @link https://www.b1.lt
 */
class B1_Accounting_Activator extends B1_Accounting
{

    /**
     * @since 2.0.0
     */
    public static function activate()
    {
        static::create_tables();
        static::init_options();
        wp_clear_scheduled_hook('admin_post_b1_sync_updated_items');
        wp_clear_scheduled_hook('admin_post_b1_sync_orders');
        wp_clear_scheduled_hook('admin_post_b1_sync_items');
    }

    public static function create_tables()
    {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        global $wpdb;

        $wpdb->query("ALTER TABLE {$wpdb->prefix}posts ADD COLUMN b1_reference_id INT NULL DEFAULT NULL");
    }

    public static function init_options()
    {
        $access_key = hash_hmac('sha256', uniqid(rand(), true), microtime() . rand());
        add_option('b1-accounting', [
            'access_key' => $access_key,
            'api_key' => '',
            'private_key' => '',
            'shop_id' => base_convert(rand(), 10, 36),
            'documentation_url' => 'https://www.b1.lt/doc/api',
            'help_page_url' => 'https://www.b1.lt/help/nuo-ko-pradeti',
            'contact_email' => 'info@b1.lt',
            'items_per_request' => 100,
            'write_off' => 0,
            'b1_initial_sync' => 0,
            'initial_product_sync_done' => 0,
            'latest_product_sync_date' => date('Y-m-d'),
            'sync_quantities' => 1,
            'sync_orders_from' => date('Y-m-d'),
            'tax_rate' => '21',

        ]);

    }


}