<?php

/**
 * @since 2.0.0
 * @package B1_Accounting
 * @subpackage B1_Accounting/includes
 * @author B1.lt <info@b1.lt>
 * @link https://www.b1.lt
 */
class B1_Accounting_Deactivator
{

    public static function deactivate()
    {
        static::drop_tables();
        static::delete_options();
        wp_clear_scheduled_hook('admin_post_b1_sync_updated_items');
        wp_clear_scheduled_hook('admin_post_b1_sync_orders');
        wp_clear_scheduled_hook('admin_post_b1_sync_items');
    }

    public static function drop_tables()
    {
        global $wpdb;
        $sql = "ALTER TABLE {$wpdb->prefix}posts DROP IF EXISTS b1_reference_id";
        $wpdb->query($sql);

    }

    public static function delete_options()
    {
        delete_option('b1-accounting');
    }

}