<?php

/**
 * @since 2.0.0
 * @package B1_Accounting
 * @subpackage B1_Accounting/includes
 * @author B1.lt <info@b1.lt>
 * @link https://www.b1.lt
 */
class B1_Accounting
{

    /**
     * @var B1_Accounting_Loader
     */
    protected $loader;
    /**
     * @var string
     */
    protected $plugin_name;
    /**
     * @var string
     */
    protected $version;

    public function __construct()
    {
        if (defined('B1_ACCOUNTING_VERSION')) {
            $this->version = B1_ACCOUNTING_VERSION;
        } else {
            $this->version = '2.0.0';
        }
        $this->plugin_name = 'b1-accounting';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
    }

    public function run()
    {
        $this->loader->run();
    }

    public function get_loader()
    {
        return $this->loader;
    }

    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    public function get_version()
    {
        return $this->version;
    }

    private function load_dependencies()
    {
        $path = plugin_dir_path(dirname(__FILE__));
        require_once $path . 'includes/lib/B1.php';
        require_once $path . 'includes/class-b1-accounting-loader.php';
        require_once $path . 'includes/class-b1-accounting-i18n.php';
        require_once $path . 'includes/class-b1-accounting-exception.php';
        require_once $path . 'includes/class-b1-accounting-helper.php';
        require_once $path . 'includes/class-b1-accounting-shop-item.php';
        require_once $path . 'includes/class-b1-accounting-order.php';
        require_once $path . 'includes/class-b1-accounting-base.php';
        require_once $path . 'includes/class-b1-accounting-manager.php';
        require_once $path . 'admin/class-b1-accounting-admin.php';
        $this->loader = new B1_Accounting_Loader();

    }

    private function set_locale()
    {
        $plugin_i18n = new B1_Accounting_i18n();
        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    public function custom_cron_schedule($schedules)
    {
        $schedules['minutes'] = array(
            'interval' => 600,
            'display' => __('Once per 10 minutes')
        );
        $schedules['2days'] = array(
            'interval' => 172800,
            'display' => __('Once in 2 days')
        );
        $schedules['week'] = array(
            'interval' => 604800,
            'display' => __('Once weekly')
        );

        return $schedules;

    }

    private function define_admin_hooks()
    {
        add_filter('cron_schedules', array($this, 'custom_cron_schedule'));

        $plugin_admin = new B1_Accounting_Admin($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
        $this->loader->add_action('admin_menu', $plugin_admin, 'add_plugin_admin_menu');

        if (!wp_next_scheduled('admin_post_b1_sync_orders')) {
            wp_schedule_event(time(), 'minutes', 'admin_post_b1_sync_orders');
        }
        $this->loader->add_action('admin_post_b1_sync_orders', $plugin_admin, 'sync_orders_handler');

        if (!wp_next_scheduled('admin_post_b1_sync_items')) {
            wp_schedule_event(time(), 'week', 'admin_post_b1_sync_items');
        }
        $this->loader->add_action('admin_post_b1_sync_items', $plugin_admin, 'sync_items_handler');

        if (!wp_next_scheduled('admin_post_b1_sync_updated_items')) {
            wp_schedule_event(time(), '2days', 'admin_post_b1_sync_updated_items');
        }
        $this->loader->add_action('admin_post_b1_sync_updated_items', $plugin_admin, 'sync_updated_items_handler');


        $plugin_basename = plugin_basename(plugin_dir_path(__DIR__) . $this->get_plugin_name() . '.php');
        $this->loader->add_filter('plugin_action_links_' . $plugin_basename, $plugin_admin, 'add_action_links');
        $this->loader->add_action('wp_ajax_b1_options_update', $plugin_admin, 'update_handler');
        $this->loader->add_action('admin_post_b1_download_invoice', $plugin_admin, 'download_invoice_handler');

    }

}
