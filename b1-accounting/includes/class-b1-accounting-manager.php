<?php

class B1_Accounting_Manager extends B1_Accounting_Base
{

    const TTL = 3600;
    const MAX_ITERATIONS = 100;
    const ORDERS_PER_ITERATION = 100;

    /**
     * @var $wpdb
     */
    private $db;
    /**
     * @var B1
     */
    private $b1;

    public function __construct($plugin_name, $version)
    {
        parent::__construct($plugin_name, $version);

        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        global $wpdb;
        $this->db = $wpdb;
    }

    public function sync_items($syncAll = true)
    {
        try {
            $this->b1 = new B1(array(
                'apiKey' => $this->get_option('api_key'),
                'privateKey' => $this->get_option('private_key'),
            ));
            $modAfterDate = $syncAll ? null : $this->get_option('latest_product_sync_date');
            $lid = null;
            $i = 0;
            if ($syncAll) {
                B1_Accounting_Shop_Item::set_null_reference_id_product();
            }
            do {
                $i++;
                $response = $this->b1->request('shop/product/list', ["lid" => $lid, "lmod" => $modAfterDate]);
                $content = $response->getContent();
                $data = $content['data'];
                foreach ($data as $item) {
                    if ($item['code'] != null) {
                        B1_Accounting_Shop_Item::update_code_using_b1_item_id($item['id'], $item['code']);
                        if ($this->get_option('sync_quantities') && $item['quantity']) {
                            B1_Accounting_Shop_Item::update_quantity_using_b1_item_id($item['quantity'], $item['id']);
                        }
                    }
                }
                if (count($data) == 100) {
                    $lid = $data[99]['id'];
                } else {
                    $lid = -1;
                }
            } while ($lid != -1 && $i < self::MAX_ITERATIONS);
            if ($syncAll) {
                $options = array_merge($this->get_options(), [
                    'initial_product_sync_done' => 1,
                ]);
                $this->update_options($options);
            } else {
                $options = array_merge($this->get_options(), [
                    'latest_product_sync_date' => date("Y-m-d"),
                ]);
                $this->update_options($options);
            }
            wp_die('OK');
        } catch (B1Exception $e) {
            B1_Accounting_Helper::debug($e);
            wp_die('Error');
        }
    }

    public function sync_orders()
    {
        $initialProductSyncDone = $this->get_option('initial_product_sync_done');
        if (!$initialProductSyncDone) {
            B1_Accounting_Helper::debug("Initial product sync is not done yet.");
            wp_die('Error');
        }
        try {
            $this->b1 = new B1(array(
                'apiKey' => $this->get_option('api_key'),
                'privateKey' => $this->get_option('private_key'),
            ));
            $sync_orders_from = $this->get_option('sync_orders_from', '2000-01-01');
            $b1_initial_sync = $this->get_option('b1_initial_sync');
            $i = 0;
            do {
                $i++;
                $orders = B1_Accounting_Order::fetch_all_orders($sync_orders_from, self::ORDERS_PER_ITERATION);
                foreach ($orders as $item) {
                    $order_data = $this->generate_order_data($item->ID);
                    try {
                        $response = $this->b1->request('shop/order/add', $order_data['data']);
                        $content = $response->getContent();

                        B1_Accounting_Order::assignB1OrderId($content['data']['orderId'], $item->ID);

                        if ($this->get_option('sync_quantities') && isset($content['data']['update'])) {
                            foreach ($content['data']['update'] as $productQuantity) {
                                B1_Accounting_Shop_Item::update_quantity_using_b1_item_id($productQuantity['quantity'], $productQuantity['id']);
                            }
                        }
                    } catch (B1DuplicateException $e) {
                        $content = $e->getResponse()->getContent();
                        B1_Accounting_Order::assignB1OrderId($content['data']['orderId'], $item->ID);
                    }
                }
            } while (count($orders) == self::ORDERS_PER_ITERATION && $i < self::MAX_ITERATIONS);
            if ($b1_initial_sync == 0) {
                $options = array_merge($this->get_options(), [
                    'b1_initial_sync' => 1,
                    'write_off' => 1
                ]);
                $this->update_options($options);
            }
            wp_die('OK');
        } catch (B1Exception $e) {
            B1_Accounting_Helper::debug($e);
            wp_die('Error');
        }
    }

    private function generate_order_data($order_id)
    {
        $order = wc_get_order($order_id);
        $order_data = array();
        $order_data['prefix'] = $this->get_option('shop_id');
        $order_data['writeOff'] = $this->get_option('write_off');
        $order_data['orderId'] = $order->get_id();
        $order_data['orderDate'] = substr($order->get_date_created(), 0, 10);
        $order_data['orderNo'] = (string)$order->get_id();
        $order_data['currency'] = $order->get_currency();
        $order_data['discount'] = intval(round(($order->get_discount_total() * 1 + $order->get_discount_tax() * 1) * 100));
        $order_data['total'] = intval(round($order->get_total() * 100));
        $order_data['orderEmail'] = $order->get_billing_email();
        $order_data['vatRate'] = intval(round($this->get_option('tax_rate')));
        $order_data['billing']['isCompany'] = $order->get_billing_company() == '' ? 0 : 1;
        $order_data['billing']['name'] = $order->get_billing_company() == '' ? trim($order->get_billing_first_name() . ' ' . $order->get_billing_last_name()) : $order->get_billing_company();
        $order_data['billing']['address'] = $order->get_billing_address_1();
        $order_data['billing']['city'] = $order->get_billing_city();
        $order_data['billing']['country'] = $order->get_billing_country();
        $order_data['delivery']['isCompany'] = $order->get_shipping_company() == '' ? 0 : 1;
        $order_data['delivery']['name'] = $order->get_shipping_company() == '' ? trim($order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name()) : $order->get_shipping_company();
        $order_data['delivery']['address'] = $order->get_shipping_address_1();
        $order_data['delivery']['city'] = $order->get_shipping_city();
        $order_data['delivery']['country'] = $order->get_shipping_country();

        if ($order_data['billing']['name'] == '') {
            $order_data['billing'] = $order_data['delivery'];
        }

        if ($order_data['delivery']['name'] == '') {
            $order_data['delivery'] = $order_data['billing'];
        }
            $order_data['shippingAmount'] = intval(round(($order->get_shipping_total() * 1 + $order->get_shipping_tax() * 1) * 100));
        /**
         * @var WC_Order_item $item
         */
        foreach ($order->get_items() as $key => $item) {
            if ($item->get_type() == 'line_item') {
                if ($item->get_variation_id() == 0) {
                    $a = B1_Accounting_Shop_Item::get_product_reference_id($item->get_product_id());
                } else {
                    $a = B1_Accounting_Shop_Item::get_variation_reference_id($item->get_variation_id());
                }

                $order_data['items'][$key]['id'] = $a;
                $order_data['items'][$key]['name'] = $item->get_name();
                $order_data['items'][$key]['quantity'] = intval(round($item->get_quantity() * 100));
                $order_data['items'][$key]['price'] = intval($order->get_item_subtotal($item, true) * 100);
                $order_data['items'][$key]['sum'] = intval($order->get_line_subtotal($item, true) * 100);
            }
        }

        return [
            'data' => $order_data
        ];
    }

    /**
     * @param WC_Order $order
     */
    public function get_invoice($order)
    {
        try {
            $this->b1 = new B1(array(
                'apiKey' => $this->get_option('api_key'),
                'privateKey' => $this->get_option('private_key'),
            ));
            $data = [
                'prefix' => $this->get_option('shop_id'),
                'orderId' => $order->get_id(),
            ];
            $response = $this->b1->request('shop/invoices/get', $data);
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename=' . $order->get_id() . '.pdf');
            echo $response->getContent();
        } catch (B1Exception $e) {
            wp_die(__('Internal error.', $this->plugin_name));
        }
    }

}
