<?php

/**
 * @since 2.0.0
 * @package B1_Accounting
 * @subpackage B1_Accounting/admin
 * @author B1.lt <info@b1.lt>
 * @link https://www.b1.lt
 */
class B1_Accounting_Admin extends B1_Accounting_Base
{

    public function enqueue_styles()
    {
        if (isset($_GET['page']) && $_GET['page'] == $this->plugin_name) {
            $plugin_dir_url = plugin_dir_url(__FILE__);
            wp_enqueue_style($this->plugin_name . '-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), $this->version, 'all');
            wp_enqueue_style($this->plugin_name . '-fa', 'https://use.fontawesome.com/releases/v5.0.6/css/all.css', array(), $this->version, 'all');
            wp_enqueue_style($this->plugin_name . '-data-table', 'https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css', array(), $this->version, 'all');
            wp_enqueue_style($this->plugin_name . '-custom', $plugin_dir_url . 'css/b1-accounting-admin.css', array(), $this->version, 'all');
        }
    }

    public function enqueue_scripts()
    {
        if (isset($_GET['page']) && $_GET['page'] == $this->plugin_name) {
            $plugin_dir_url = plugin_dir_url(__FILE__);
            wp_enqueue_script($this->plugin_name . '-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
            wp_enqueue_script($this->plugin_name . '-data-table', 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js', array('jquery'), $this->version, false);
            wp_enqueue_script($this->plugin_name . '-data-table-bootstrap', 'https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js', array('jquery'), $this->version, false);
            wp_register_script($this->plugin_name . '-custom', $plugin_dir_url . 'js/b1-accounting-admin.js', array('jquery'), $this->version, false);
            $custom = array(
                'security' => wp_create_nonce('b1_security'),
                'base_url' => admin_url() . 'admin-ajax.php',
                'texts' => [
                    'internal_error' => __('Internal error.', $this->plugin_name),
                    'processing' => __('Processing...', $this->plugin_name),
                    'length_menu' => __('Show _MENU_ items', $this->plugin_name),
                    'zero_records' => __('Nothing found', $this->plugin_name),
                    'showing_records' => __('Showing _START_ to _END_ of _TOTAL_ entries', $this->plugin_name),
                    'showing_zero_records' => __('No data available in table', $this->plugin_name),
                    'first' => __('First', $this->plugin_name),
                    'previous' => __('Previous', $this->plugin_name),
                    'next' => __('Next', $this->plugin_name),
                    'last' => __('Last', $this->plugin_name),
                ],
            );
            wp_localize_script($this->plugin_name . '-custom', 'b1', $custom);
            wp_enqueue_script($this->plugin_name . '-custom');
        }
    }


    public function add_plugin_admin_menu()
    {
        add_submenu_page('woocommerce', 'B1.lt', 'B1.lt', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page'));
    }

    public function display_plugin_setup_page()
    {
        include_once('partials/b1-accounting-admin-dashboard.php');
    }

    public function add_action_links($links)
    {
        $settingsLink = array(
            '<a href="' . admin_url('admin.php?page=' . $this->plugin_name) . '">' . __('Settings', $this->plugin_name) . '</a>',
        );
        return array_merge($settingsLink, $links);
    }

    public function update_handler()
    {
        if (isset($_POST['b1_security']) && wp_verify_nonce($_POST['b1_security'], 'b1_security')) {
            $keys = array('shop_id', 'sync_orders_from', 'sync_quantities', 'tax_rate', 'api_key', 'private_key', 'access_key');
            $data = array();
            foreach ($keys as $key) {
                $data[$key] = isset($_POST[$key]) ? sanitize_text_field(trim($_POST[$key])) : null;
            }
            $data['sync_quantities'] = intval($data['sync_quantities']);
            $data['sync_orders_from'] = empty($data['sync_orders_from']) ? null : $data['sync_orders_from'];
            $data['api_key'] = empty($data['api_key']) ? null : $data['api_key'];
            $data['private_key'] = empty($data['private_key']) ? null : $data['private_key'];
            $data['access_key'] = empty($data['access_key']) ? null : $data['access_key'];

            if (empty($data['shop_id'])) {
                B1_Accounting_Helper::sendErrorResponse(__('Shop ID cannot be empty.', $this->plugin_name));
            }
            if (!empty($data['sync_orders_from']) && DateTime::createFromFormat('Y-m-d', $data['sync_orders_from']) === false) {
                B1_Accounting_Helper::sendErrorResponse(__('Bad sync orders from date format. Should be "yyyy-mm-dd".', $this->plugin_name));
            }
            if (empty($data['tax_rate'])) {
                B1_Accounting_Helper::sendErrorResponse(__('Tax rate cannot be empty.', $this->plugin_name));
            }
            if (empty($data['access_key'])) {
                B1_Accounting_Helper::sendErrorResponse(__('Access key cannot be empty.', $this->plugin_name));
            } else if (strlen($data['access_key']) < 16) {
                B1_Accounting_Helper::sendErrorResponse(__('Access key length cannot be less than 16.', $this->plugin_name));
            }

            $options = array_merge($this->get_options(), $data);
            $this->update_options($options);

            B1_Accounting_Helper::sendSuccessResponse(__('Settings saved successfully!', $this->plugin_name));
        } else {
            B1_Accounting_Helper::sendErrorResponse(__('Invalid secret key specified.', $this->plugin_name));
        }
    }

    public function sync_updated_items_handler()
    {
        $manager = new B1_Accounting_Manager($this->plugin_name, $this->version);
        $manager->sync_items(false);
    }

    public function sync_items_handler()
    {
        $manager = new B1_Accounting_Manager($this->plugin_name, $this->version);
        $manager->sync_items();
    }

    public function sync_orders_handler()
    {
        $manager = new B1_Accounting_Manager($this->plugin_name, $this->version);
        $manager->sync_orders();
    }

    public function download_invoice_handler()
    {
        $key = isset($_GET['key']) ? sanitize_text_field($_GET['key']) : -1;
        $order_id = isset($_GET['order_id']) ? sanitize_text_field($_GET['order_id']) : -1;
        $order = wc_get_order($order_id);
        if (!$order) {
            wp_die(__('Forbidden', $this->plugin_name));
        } else if ($order->post->post_password != $key) {
            wp_die(__('Forbidden', $this->plugin_name));
        } else {
            $manager = new B1_Accounting_Manager($this->plugin_name, $this->version);
            $manager->get_invoice($order);
        }
    }

}