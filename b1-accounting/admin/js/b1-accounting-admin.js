jQuery(document).ready(function ($) {

    "use strict";

    var b1_error_alert = $("#b1-error-alert");
    var b1_success_alert = $("#b1-success-alert");

    function b1_show_success_alert(message) {
        b1_error_alert.addClass('hidden');
        b1_success_alert.find('p').text(message);
        b1_success_alert.removeClass('hidden');
    }

    function b1_show_error_alert(message) {
        b1_success_alert.addClass('hidden');
        b1_error_alert.find('p').text(message);
        b1_error_alert.removeClass('hidden');
    }

    $('#form-settings').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: b1.base_url,
            type: 'post',
            data: $(this).serialize()
        }).done(function (response) {
            if (response.success) {
                b1_show_success_alert(response.message);
            } else {
                b1_show_error_alert(response.message);
            }
        }).fail(function () {
            b1_show_error_alert(b1.internal_error);
        });
    });

    $(document).on("click", "tr", function (e) {
        $(':checkbox', this).trigger('click');
        $(':radio', this).prop("checked", true);
    });

    $.extend(true, $.fn.dataTable.defaults, {
        processing: true,
        serverSide: true,
        ordering: false,
        pageLength: 50,
        initComplete: function () {
            var api = this.api();
            api.columns().every(function () {
                var that = this;
                $('select', this.header()).on('change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        },
        language: {
            sProcessing: b1.texts.processing,
            sLengthMenu: b1.texts.length_menu,
            sZeroRecords: b1.texts.zero_records,
            Info: b1.texts.showing_records,
            sInfoEmpty: b1.texts.showing_zero_records,
            oPaginate: {
                sFirst: '<<',
                sPrevious: '<',
                sNext: '>',
                sLast: '>>'
            }
        }
    });
    $.fn.dataTableExt.oStdClasses.sWrapper = 'dataTables_wrapper dt-bootstrap no-footer';

});