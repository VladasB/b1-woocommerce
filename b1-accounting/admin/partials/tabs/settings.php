<?php
if (!defined('WPINC')) {
    die;
}
/**
 * @var B1_Accounting_Admin $this
 */
$path = admin_url() . 'admin-post.php?action=';
$manager_urls = array(
    array(
        'url' => $path . 'b1_sync_items&key=' . $this->get_option('access_key'),
    ),
    array(
        'url' => $path . 'b1_sync_orders&key=' . $this->get_option('access_key'),
    ),
    array(
        'url' => $path . 'b1_sync_updated_items&key=' . $this->get_option('access_key'),
    ),
);
$nonce = wp_create_nonce('b1_security');
?>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fas fa-cogs"></i> <?php _e('Configuration', $this->plugin_name); ?>
                </h3>
            </div>
            <div class="panel-body">
                <form class="form" id="form-settings">
                    <input class="form-control" name="action" value="b1_options_update" type="hidden">
                    <input type="hidden" name="b1_security" value="<?php echo $nonce ?>"/>
                    <div class="form-group">
                        <label for="shop_id"><?php _e('E-shop ID', $this->plugin_name); ?></label>
                        <input class="form-control" name="shop_id" value="<?php echo $this->get_option('shop_id'); ?>" type="text">
                    </div>
                    <div class="form-group">
                        <label for="sync_orders_from"><?php _e('Sync orders from', $this->plugin_name); ?></label>
                        <input class="form-control" name="sync_orders_from" value="<?php echo $this->get_option('sync_orders_from'); ?>" type="text">
                    </div>
                    <div class="form-group">
                        <label for="sync_quantities"><?php _e('Sync B1.lt quantities with e-shop', $this->plugin_name); ?></label>
                        <select class="form-control" name="sync_quantities">
                            <option value="1" <?php echo ($this->get_option('sync_quantities')) ? 'selected' : ''; ?>>
                                <?php _e('Yes', $this->plugin_name); ?>
                            </option>
                            <option value="0" <?php echo (!$this->get_option('sync_quantities')) ? 'selected' : ''; ?>>
                                <?php _e('No', $this->plugin_name); ?>
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tax_rate"><?php _e('VAT tax rate', $this->plugin_name); ?></label>
                        <input class="form-control" name="tax_rate" value="<?php echo $this->get_option('tax_rate'); ?>" type="text">
                    </div>
                    <div class="form-group">
                        <label for="api_key"><?php _e('API key', $this->plugin_name); ?></label>
                        <input class="form-control" name="api_key" value="<?php echo $this->get_option('api_key'); ?>" type="text">
                    </div>
                    <div class="form-group">
                        <label for="private_key"><?php _e('Private key', $this->plugin_name); ?></label>
                        <input class="form-control" name="private_key" value="<?php echo $this->get_option('private_key'); ?>" type="text">
                    </div>
                    <div class="form-group">
                        <label for="access_key"><?php _e('Access key', $this->plugin_name); ?></label>
                        <input class="form-control" name="access_key" value="<?php echo $this->get_option('access_key'); ?>" type="text">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            <i class="fas fa-cloud-upload-alt"></i> <?php _e('Update', $this->plugin_name); ?>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fas fa-redo"></i> <?php _e('Commands', $this->plugin_name); ?></h3>
            </div>
            <div class="list-group">
                <?php foreach ($manager_urls as $data) { ?>
                    <div class="list-group-item">
                        <div class="input-group">
                            <input type="text" class="form-control" value="<?php echo $data['url'] ?>">
                            <div class="input-group-btn">
                                <a href="<?php echo $data['url'] ?>" type="button " target="_blank" class="btn btn-primary">
                                    <i class="fas fa-play-circle" aria-hidden="true"></i> <?php _e('Run', $this->plugin_name); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>