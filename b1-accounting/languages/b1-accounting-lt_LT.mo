��    O      �  k         �     �  
   �     �  )   �  )     *   <  .   g     �     �     �  9   �                         ,     5  	   C     M     Z  	   `     j     o  
   r     }     �     �     �  
   �     �     �     �     �  0   	     3	     M	     Y	     f	     k	     x	     }	     �	     �	     �	     �	     �	     �	     �	     �	     
     $
     (
     1
     N
  "   g
     �
  +   �
     �
  &   �
     �
          /     O  !   k  !   �  !   �     �     �  $   �     !     7     S     p     w     �     �     �     �  j  �  
        "  '   2  .   Z     �  (   �  2   �     �            4   0     e     k     y     �  	   �     �     �     �     �     �  
   �     �  
   �               +  %   ;  	   a     k     �     �     �  A   �  #   �  D        W     i     u     �     �     �     �     �     �     �     �  #   �          )     D  
   M  0   X  '   �  !   �     �  :   �  
   %  3   0     d     �     �     �     �     �     �       #   0     T     m          �     �    �     /  
   C     N     Z         O      <   L       #       D   C   3             J   	   7          '   1   I   F                          5   ?   4   
          $       -   B       ;   K              +                  G      E      M          )      @          A   =                       !      &   >   *      8   :   ,         .         6   (          2         %       9   /       0   N                 H   "            API key Access key Access key cannot be empty. Access key length cannot be less than 16. Are you sure you want to reset all links? Are you sure you want to unlink this item? At least one item from B1.lt must be selected. B1.lt accounting B1.lt for WooCommerce B1.lt items Bad sync orders from date format. Should be "yyyy-mm-dd". Code Code (B1.lt) Commands Configuration Contacts Documentation E-shop ID E-shop items First Forbidden Help ID ID (B1.lt) ID (E-shop) Integration with B1.lt Internal error. Invalid secret key specified. Is linked? Items in B1.lt Items in e-shop Last Link Link all items between e-shop and B1.lt by code? Link all products by code Link manual Linked items Name Name (B1.lt) Next No No data available in table Not synced orders Nothing found Previous Private key Processing... Quantity sync is disabled. Reset all item links Reset all order links Run Settings Settings saved successfully! Shop ID cannot be empty. Shop item ID item cannot be empty. Show _MENU_ items Showing _START_ to _END_ of _TOTAL_ entries Stats Successfully linked all items by code. Successfully linked items. Successfully reset item links. Successfully reset order links. Successfully unlinked item. Sync B1.lt item codes with e-shop Sync B1.lt item names with e-shop Sync B1.lt quantities with e-shop Sync orders from Tax rate cannot be empty. Try to write off when syncing orders Unable to link items. Unable to reset item links. Unable to reset order links. Unlink Unlink manual Unlinked items Update VAT tax rate Yes Project-Id-Version: B1.lt for WooCommerce 2.0.0
Report-Msgid-Bugs-To: http://wordpress.org/support/plugin/b1-accounting
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
X-Generator: grunt-wp-i18n 0.4.9
 API raktas Prieigos raktas Prieigos raktas negali būti tuščias. Prieigos raktas negali būti mažesnis nei 16. Ar tikrai norite atsieti? Ar tikrai norite atsieti šį produktą? Reikia pasirinkti bent vieną produktą iš B1.lt. B1.lt buhalterija B1.lt B1.lt sistemos produktai Blogas datos formatas. Turėtų būti "yyyy-mm-dd ". Kodas Kodas (B1.lt) Komandos Konfigūracija Kontaktai B1 API dokumentacija El. parduotuvės ID El. parduotuvės produktai << Draudžiama B1 pagalba ID ID (B1.lt) ID (El. pard.) Integracija su B1.lt Vidinė klaida. Nurodytas neteisingas saugumo raktas. Susietas? Produktai B1.lt sistemoje Produktai el. parduotuvėje >> Susieti Susieti visus produktus tarp e-parduotuvės ir B1.lt pagal kodą? Susieti visus produktus pagal kodą Išveda pozicijas, kurios yra sujungtos tarp el. parduotuvės ir B1. Susiję produktai Pavadinimas Pavadinimas (El. pard.) > Ne Nieko nerasta Nesinchronizuoti užsakymai Įrašų nerasta < Privatus raktas Apdorojama... Kiekio sinchronizavimas išjungtas. Atsieti visus produktus Atstatyti visus užsakymus Paleisti Nustatymai Konfigūravimo nustatymai išsaugoti sėkmingai! Parduotuvės ID negali būti tuščias. Produktas negali būti tuščias. Rodyti _MENU_ įrašus Rodomi įrašai nuo _START_ iki _END_ iš _TOTAL_ įrašų Statistika Visi produktai buvo sėkmingai susieti pagal kodą. Sėkmingai susieti produktai. Sėkmingai atsatyti. Sėkmingai atsatyti. Sėkmingai atsietas produktas. Sinchronizuoti kodus Sinchronizuoti pavadinimus Sinchronizuoti kiekius Sinchronizuoti užsakymus nuo PVM tarifas negali būti tuščias. Bandyti nurašyti kiekį Nepavyko susieti. Nepavyko atstatyti. Nepavyko atstatyti. Atsieti Rodomos pozicijos, kurie yra el. parduotuvėje ir B1 sistemoje ir nėra sujungti tarpusavyje. Kiekviena nauja pozicija įtraukta į el.parduotuvę arba į B1 sistemą turėtų būti rankiniu būdu susiejama čia. Norėdami susieti pozicijas:<br>1. Pasirinkite poziciją iš el. parduotuvės sąrašo;<br>2. Pasirinkite poziciją iš B1 sąrašo;<br>3. Paspauskite mygtuką "Susieti"; Nesusiję produktai Atnaujinti PVM tarifas Taip 